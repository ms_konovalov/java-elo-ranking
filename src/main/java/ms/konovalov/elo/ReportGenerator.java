package ms.konovalov.elo;

import de.vandermeer.asciitable.AsciiTable;
import de.vandermeer.asciitable.CWC_LongestLine;

import java.util.Arrays;
import java.util.List;

/**
 * Generates reports in string representation
 */
public class ReportGenerator {

    /*
     * Generate text report for full rank data
     */
    public static String printFullRankReport(PlayerRankWithPosition[] report) {
        if (report.length == 0) {
            return "Players rating is empty!";
        }

        AsciiTable table = new AsciiTable();

        table.addRule();
        table.addRow("Rank", "Name", "Score", "W/L");
        table.addRule();

        Arrays.stream(report).forEachOrdered(rank ->
                table.addRow(rank.getPosition(), getFullName(rank.getPlayer()), rank.getRating(), rank.getWinsCount() + "/" + rank.getLosesCount())
        );
        table.addRule();
        table.getRenderer().setCWC(new CWC_LongestLine());

        return table.render();
    }

    /*
     * Generate text report for Player history
     */
    public static String printPersonalReport(PlayerRankWithPosition playerRank) {
        if (playerRank == null) {
            return "Player could not be found!";
        }

        AsciiTable table = new AsciiTable();

        table.addRule();
        table.addRow("Name", null, null, getFullName(playerRank.getPlayer()));

        table.addRule();
        table.addRow("Rank", playerRank.getPosition(), "Rating", playerRank.getRating());

        table.addRule();
        List<Player> wins = playerRank.getWins();
        List<Player> loses = playerRank.getLoses();

        table.addRow("Wins", getFullName(getOrNull(wins, 0)), "Loses", getFullName(getOrNull(loses, 0)));

        for (int i = 1; i < Math.max(playerRank.getWinsCount(), playerRank.getLosesCount()); i++) {
            table.addRow("", getFullName(getOrNull(wins, i)), "", getFullName(getOrNull(loses, i)));
        }

        table.addRule();
        table.getRenderer().setCWC(new CWC_LongestLine());

        return table.render();
    }

    /*
     * Generate text report for recommended matches
     */
    public static String printRecommendedMatches(Match[] recommended) {
        if (recommended.length == 0) {
            return "Unable to recommend any match!";
        }

        AsciiTable table = new AsciiTable();

        table.addRule();
        for (int i = 0; i < recommended.length; i++) {
            table.addRow(i + 1, getFullName(recommended[i].getWinner()), getFullName(recommended[i].getLoser()));
        }
        table.addRule();
        table.getRenderer().setCWC(new CWC_LongestLine());

        return table.render();
    }

    private static Player getOrNull(List<Player> coll, int idx) {
        if (coll.size() - 1 < idx) {
            return null;
        }
        return coll.get(idx);
    }

    private static String getFullName(Player p) {
        if (p == null)
            return "";
        return "(ID=" + p.getId() + ") " + p.getName();
    }
}
