package ms.konovalov.elo;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

/**
 * Main console interface for Elo rating calculator
 */
public class EloRankApplication {

    public static void main(String[] args) {
        EloRankApplication main = new EloRankApplication();
        InitialParam param = new InitialParam();
        JCommander jCommander = JCommander.newBuilder()
                .addObject(param)
                .build();
        jCommander.setProgramName("EloRankApplication");
        jCommander.parse(args);

        if (param.help) {
            jCommander.usage();
            System.exit(0);
        }

        RankCalculator calc = null;
        try {
            calc = main.run(param);
        } catch (IOException e) {
            JCommander.getConsole().println("Unable to read file");
            System.exit(1);
        }

        PersonalCommand personal = new PersonalCommand();
        JCommander build = JCommander.newBuilder()
                .addCommand("full", new FullCommand())
                .addCommand("personal", personal)
                .addCommand("recommended", new RecommendedCommand())
                .addCommand("exit", new ExitCommand())
                .build();

        while (true) {

            JCommander.getConsole().println("");
            StringBuilder out = new StringBuilder("Choose which report do you want to print:\n");
            printCommands(build, out);

            JCommander.getConsole().println(out.toString());

            Scanner scanner = new Scanner(System.in);
            String s = scanner.nextLine();
            build.parse(s.split(" "));

            switch (build.getParsedCommand()) {
                case "exit":
                    System.exit(0);
                    break;
                case "full":
                    PlayerRankWithPosition[] report = calc.generateFullRankReport();
                    JCommander.getConsole().println(ReportGenerator.printFullRankReport(report));
                    break;
                case "personal":
                    Optional<PlayerRankWithPosition> personalRank = calc.generatePersonalReport(personal.id);
                    if (personalRank.isPresent()) {
                        JCommander.getConsole().println(ReportGenerator.printPersonalReport(personalRank.get()));
                    } else {
                        JCommander.getConsole().println("Unable to find player with id=" + personal.id);
                    }
                    break;
                case "recommended":
                    Match[] x = calc.generateSuggestedMatches();
                    JCommander.getConsole().println(ReportGenerator.printRecommendedMatches(x));
                    break;
            }
        }
    }

    private static void printCommands(JCommander build, StringBuilder out) {
        out.append("   Commands:\n");

        for (Map.Entry<String, JCommander> commands : build.getCommands().entrySet()) {
            String description = build.getCommandDescription(commands.getKey());
            if (description != null) {
                out.append("    ").append(commands.getKey()).append("      ").append(description).append("\n");
            }

            JCommander jc = commands.getValue();
            jc.usage(out, "      ");
            out.append("\n");
        }
    }

    private RankCalculator run(InitialParam param) throws IOException {
        return new RankCalculatorFileWrapper().calculate(Paths.get(param.playersFilePath), Paths.get(param.matchesFilePath), param.initialRating, param.kFactor);
    }

    @Parameters(separators = "=")
    private static class InitialParam {
        @Parameter(names = {"--players", "-p"}, required = true, description = "path to file with list of players")
        private String playersFilePath;
        @Parameter(names = {"--matches", "-m"}, required = true, description = "path to file with list of played matches")
        private String matchesFilePath;
        @Parameter(names = {"--initialRating", "-i"}, description = "initial rating for new players")
        private int initialRating = RankCalculator.DEFAULT_INITIAL_RANK;
        @Parameter(names = {"--kfactor", "-k"}, description = "K factor for Elo algorithm")
        private int kFactor = RankCalculator.DEFAULT_K_FACTOR;
        @Parameter(names = "--help", help = true)
        private boolean help;
    }

    @Parameters(commandDescription = "print full rating report")
    private static class FullCommand {
    }

    @Parameters(commandDescription = "print personal rating report")
    private static class PersonalCommand {
        @Parameter(description = "{player_id}")
        private String id;
    }

    @Parameters(commandDescription = "print recommended matches")
    private static class RecommendedCommand {
    }

    @Parameters(commandDescription = "exit")
    private static class ExitCommand {
    }

}
