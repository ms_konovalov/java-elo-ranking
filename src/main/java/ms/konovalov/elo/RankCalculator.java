package ms.konovalov.elo;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Elo rank calculator
 */
public class RankCalculator {

    public static final int DEFAULT_INITIAL_RANK = 1000;
    public static final int DEFAULT_K_FACTOR = 32;
    private static final int WIN = 1;
    private static final int LOOSE = 0;

    private final int initialRank;
    private final int kFactor;
    private final Map<Player, PlayerRank> players;
    private PlayerRankWithPosition[] cache;

    /**
     * Constructor
     *
     * @param players initial set of {@link Player}s in tournament
     * @param initialRank starting rank for each player in this calculation
     * @param kFactor K-factor for Elo-algorythm for current calculation
     */
    public RankCalculator(Set<Player> players, int initialRank, int kFactor) {
        this.initialRank = initialRank;
        this.kFactor = kFactor;
        this.players = players.stream().collect(Collectors.toMap(Function.identity(), this::createInitialRank));
    }

    /**
     * Add player to tournament with initial rating
     *
     * @param player player
     */
    public void addPlayer(Player player) {
        invalidateCache();
        players.putIfAbsent(player, createInitialRank(player));
    }

    private PlayerRank createInitialRank(Player p) {
        return new PlayerRank(p, initialRank);
    }

    /*
     * Add mathc and recalculate rating of paticipants
     */
    private void addMatch(Match match) {
        invalidateCache();

        PlayerRank winner = players.get(match.getWinner());
        PlayerRank loser = players.get(match.getLoser());

        NewRating newRatings = calculateNewRating(winner.getRating(), loser.getRating());

        winner.addWin(match.getLoser());
        winner.setRating(newRatings.getPlayer1());

        loser.addLose(match.getWinner());
        loser.setRating(newRatings.getPlayer2());
    }

    /*
     * Calculate new rating for match participants according to their previous rating
     */
    private NewRating calculateNewRating(int winnerRating, int loserRating) {
        /*
         * Expected rating for a user:
         *
         * e(i) = q(i) / (q(1) + q(2))
         *
         * where
         *
         * q(i) = 10 ^ (r(i) / 400)
         */
        double q1 = Math.pow(10, winnerRating / 400);
        double q2 = Math.pow(10, loserRating / 400);
        double e1 = q1 / (q1 + q2);
        double e2 = q2 / (q1 + q2);

        /*
         * new rating(i) = r(i) + kFactor * (S(i) - e(i));
         *
         * where S - actual score 1 for win, 0 for lose
         */
        int newRating1 = (int) Math.round(winnerRating + kFactor * (WIN - e1));
        int newRating2 = (int) Math.round(loserRating + kFactor * (LOOSE - e2));

        return new NewRating(newRating1, newRating2);
    }

    /**
     * Add stream of {@link Match}es and recalculate rating according to these matches
     *
     * @param matchesStream stream of matches
     */
    public void addMatches(Stream<Match> matchesStream) {
        matchesStream.forEachOrdered(this::addMatch);
    }

    private void invalidateCache() {
        cache = null;
    }

    /*
     * Sort PLayers' rank and store sorted to cache
     */
    private PlayerRankWithPosition[] getFromCacheOrCalculate() {
        if (cache == null) {
            AtomicInteger count = new AtomicInteger(0);
            cache = players.values().stream()
                    .sorted(Comparator
                            .comparingInt(PlayerRank::getRating).reversed()
                            .thenComparing(Comparator.comparingInt(PlayerRank::getWinsCount).reversed()))
                    .map(r -> new PlayerRankWithPosition(count.incrementAndGet(), r))
                    .toArray(PlayerRankWithPosition[]::new);
        }
        return cache;
    }

    /**
     * Return full rating data for printing
     *
     * @return sorted array of players' rating
     */
    public PlayerRankWithPosition[] generateFullRankReport() {
        return getFromCacheOrCalculate();
    }

    /**
     * Return Rating for provided {@link Player}
     *
     * @param playerId player identifier
     * @return optional value, empty if player not found
     */
    public Optional<PlayerRankWithPosition> generatePersonalReport(String playerId) {
        return Arrays.stream(getFromCacheOrCalculate()).filter(p -> p.getPlayer().getId().equals(playerId)).findFirst();
    }

    /**
     * Generates list of recommended matches according to current {@link Player}s' rank
     * It tryes to combine participants with similar rating but tries to exclude matches that already happend
     * If there is no available not played matches just following on the rating player will be chosen
     *
     * @return array of recommended matches
     */
    public Match[] generateSuggestedMatches() {
        List<Match> matches = new ArrayList<>();
        PlayerRankWithPosition[] fromCache = getFromCacheOrCalculate();
        PlayerRankWithPosition[] copy = Arrays.copyOf(fromCache, fromCache.length);
        for (int i = 0; i < copy.length - 1; i++) {
            if (copy[i] != null) {
                boolean found = false;
                for (int j = i + 1; j < copy.length; j++) {
                    if (copy[j] != null && recommended(copy[i], copy[j])) {
                        recommendMatch(matches, copy, i, j);
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    for (int j = i + 1; j < copy.length; j++) {
                        if (copy[j] != null) {
                            recommendMatch(matches, copy, i, j);
                            break;
                        }
                    }
                }
            }
        }
        return matches.toArray(new Match[matches.size()]);
    }

    private void recommendMatch(List<Match> matches, PlayerRankWithPosition[] copy, int i, int j) {
        matches.add(new Match(copy[i].getPlayer(), copy[j].getPlayer()));
        copy[i] = null;
        copy[j] = null;
    }

    /*
     * Match considered as recommended if it was not played earlier
     */
    private boolean recommended(PlayerRankWithPosition p1, PlayerRankWithPosition p2) {
        return !p1.playedWith(p2.getPlayer());
    }

    @RequiredArgsConstructor
    @Getter
    private static class NewRating {
        private final int player1;
        private final int player2;
    }
}
