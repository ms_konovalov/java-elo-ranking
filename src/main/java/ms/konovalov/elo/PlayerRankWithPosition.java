package ms.konovalov.elo;

import lombok.Delegate;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Container for current {@link PlayerRank} with position in sorted list
 */
@RequiredArgsConstructor
public class PlayerRankWithPosition {

    @Getter private final int position;
    @Delegate private final PlayerRank rank;
}
