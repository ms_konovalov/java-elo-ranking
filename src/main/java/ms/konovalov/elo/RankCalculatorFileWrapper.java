package ms.konovalov.elo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Wrapper for {@link RankCalculator} that takes input data from files
 */
public class RankCalculatorFileWrapper {

    private Map<String, Player> playersMap;

    /**
     * Take data from provided files and calculate rating
     *
     * @param playersFile file with list of players
     * @param matchesFile file with list of played matches
     * @return calculator
     * @throws IOException if file cannot de read
     */
    public RankCalculator calculate(Path playersFile, Path matchesFile, int initialRating, int kFactor) throws IOException {
        Set<Player> players = Files.lines(playersFile).filter(line -> !line.isEmpty()).map(this::parsePlayersLine).collect(Collectors.toSet());
        playersMap = players.stream().collect(Collectors.toMap(Player::getId, Function.identity()));
        RankCalculator rankCalculator = new RankCalculator(players, initialRating, kFactor);
        Stream<Match> matchesStream = Files.lines(matchesFile).filter(line -> !line.isEmpty()).map(line -> parseMatchLine(line, playersMap));
        rankCalculator.addMatches(matchesStream);
        return rankCalculator;
    }

    private Player parsePlayersLine(String line) {
        String[] split = line.split(" ");
        if (split.length != 2) {
            throw new IllegalArgumentException("Players file is corrupted");
        }
        return new Player(split[0], split[1]);
    }

    private Match parseMatchLine(String line, Map<String, Player> players) {
        String[] split = line.split(" ");
        if (split.length != 2) {
            throw new IllegalArgumentException("Matches file is corrupted");
        }
        Player winner = getPlayer(players, split[0]);
        Player loser = getPlayer(players, split[1]);

        return new Match(winner, loser);
    }

    private Player getPlayer(Map<String, Player> players, String key) {
        if (!players.containsKey(key)) {
            throw new IllegalArgumentException("Player with id=" + key + "is not defined");
        }
        return players.get(key);
    }
}
