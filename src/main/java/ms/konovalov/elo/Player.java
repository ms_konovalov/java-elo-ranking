package ms.konovalov.elo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Class representing Player
 */
@RequiredArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Player {

    @Getter private final String id;
    @Getter private final String name;
}
