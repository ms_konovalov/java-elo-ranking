package ms.konovalov.elo;

import lombok.Getter;

import java.util.Objects;

/**
 * Class representing match between 2 {@link Player}s
 */
@Getter
public class Match {

    private final Player winner;
    private final Player loser;

    public Match(Player winner, Player loser) {
        Objects.requireNonNull(winner, "Winner must not be null");
        Objects.requireNonNull(loser, "Loser must not be null");
        if (Objects.equals(winner, loser)) {
            throw new IllegalArgumentException("Player cannot play with himself");
        }
        this.winner = winner;
        this.loser = loser;
    }
}
