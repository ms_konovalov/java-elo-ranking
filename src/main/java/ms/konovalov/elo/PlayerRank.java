package ms.konovalov.elo;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class to store current rating of one {@link Player}
 */
public class PlayerRank {

    @Getter private final Player player;

    /** rating in points */
    @Getter @Setter private int rating;

    /** List of {@link ms.konovalov.elo.Player}s he won */
    private final List<Player> wins = new ArrayList<>();

    /** List of {@link ms.konovalov.elo.Player}s he lost */
    private final List<Player> loses = new ArrayList<>();

    public PlayerRank(Player player, int initialRating) {
        this.player = player;
        this.rating = initialRating;
    }

    public void addWin(Player loser) {
        wins.add(loser);
    }

    public void addLose(Player winner) {
        loses.add(winner);
    }

    public List<Player> getWins() {
        return Collections.unmodifiableList(wins);
    }

    public int getWinsCount() {
        return wins.size();
    }

    public List<Player> getLoses() {
        return Collections.unmodifiableList(loses);
    }

    public int getLosesCount() {
        return loses.size();
    }

    public boolean playedWith(Player p) {
        return wins.contains(p) || loses.contains(p);
    }

}
