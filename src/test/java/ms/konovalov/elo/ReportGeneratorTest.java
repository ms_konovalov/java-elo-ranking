package ms.konovalov.elo;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ReportGeneratorTest {

    @Test
    public void testPrintFullRankReportEmpty() {
        String report = ReportGenerator.printFullRankReport(new PlayerRankWithPosition[0]);

        System.out.println(report);
        assertTrue(!report.isEmpty());
        assertTrue(report.contains("Players rating is empty!"));
    }

    @Test
    public void testPrintFullRankReportFull() {
        PlayerRankWithPosition[] rating = new PlayerRankWithPosition[]{
                new PlayerRankWithPosition(1, new PlayerRank(new Player("123", "Ivan"), 100)),
                new PlayerRankWithPosition(1, new PlayerRank(new Player("123", "Maria"), 200))
        };
        String report = ReportGenerator.printFullRankReport(rating);

        System.out.println(report);
        assertTrue(!report.isEmpty());
        assertTrue(report.contains("Ivan"));
        assertTrue(report.contains("Maria"));
    }

    @Test
    public void testPrintPersonalReportEmpty() {
        String report = ReportGenerator.printPersonalReport(null);

        System.out.println(report);
        assertTrue(!report.isEmpty());
        assertTrue(report.contains("Player could not be found!"));
    }

    @Test
    public void testPrintPersonalReportFull() {
        PlayerRank ivan = new PlayerRank(new Player("123", "Ivan"), 2300);
        ivan.addLose(new Player("222", "John Doe"));
        ivan.addWin(new Player("333", "T-Rex"));
        ivan.addWin(new Player("444", "Lewis"));
        String report = ReportGenerator.printPersonalReport(new PlayerRankWithPosition(137, ivan));

        System.out.println(report);
        assertTrue(!report.isEmpty());
        assertTrue(report.contains("Ivan"));
        assertTrue(report.contains("T-Rex"));
        assertTrue(report.contains("John Doe"));
        assertTrue(report.contains("Lewis"));
    }

    @Test
    public void testPrintRecommendedMatchesEmpty() {
        String report = ReportGenerator.printRecommendedMatches(new Match[0]);

        System.out.println(report);
        assertTrue(!report.isEmpty());
        assertTrue(report.contains("Unable to recommend any match!"));
    }

    @Test
    public void testPrintRecommendedMatchesFull() {
        String report = ReportGenerator.printRecommendedMatches(new Match[]{
                new Match(new Player("1", "Ivan"), new Player("2", "Maria")),
                new Match(new Player("36", "Peter"), new Player("72", "Casey")),
                new Match(new Player("27", "John Doe"), new Player("22", "Harry Potter"))
        });

        System.out.println(report);
        assertTrue(!report.isEmpty());
        assertTrue(report.contains("Ivan"));
        assertTrue(report.contains("Maria"));
        assertTrue(report.contains("Peter"));
        assertTrue(report.contains("Casey"));
        assertTrue(report.contains("John Doe"));
        assertTrue(report.contains("Harry Potter"));
    }
}