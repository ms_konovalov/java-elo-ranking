package ms.konovalov.elo;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static ms.konovalov.elo.RankCalculator.*;

@SuppressWarnings("ConstantConditions")
public class RankCalculatorFileWrapperTest {

    private RankCalculatorFileWrapper fileWrapper = new RankCalculatorFileWrapper();
    private Path players = Paths.get(getClass().getClassLoader().getResource("players.txt").toURI());
    private Path playersEmpty = Paths.get(getClass().getClassLoader().getResource("players_empty.txt").toURI());
    private Path playersCorrupted = Paths.get(getClass().getClassLoader().getResource("players_corrupted.txt").toURI());
    private Path matches = Paths.get(getClass().getClassLoader().getResource("matches.txt").toURI());
    private Path matchesCorrupted = Paths.get(getClass().getClassLoader().getResource("matches_corrupted.txt").toURI());

    public RankCalculatorFileWrapperTest() throws URISyntaxException {
    }

    @Test
    public void test() throws URISyntaxException, IOException {
        RankCalculator calculator = fileWrapper.calculate(players, matches, DEFAULT_INITIAL_RANK, DEFAULT_K_FACTOR);
        PlayerRankWithPosition[] report = calculator.generateFullRankReport();

        Assert.assertEquals("36", report[0].getPlayer().getId());
        Assert.assertTrue(getPlayerPositionByID(report, "4") < getPlayerPositionByID(report, "3"));

    }

    @Test(expected = IllegalArgumentException.class)
    public void testPlayersCorrupted() throws URISyntaxException, IOException {
        RankCalculator calculator = fileWrapper.calculate(playersCorrupted, matches, DEFAULT_INITIAL_RANK, DEFAULT_K_FACTOR);
        calculator.generateFullRankReport();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMatchesCorrupted() throws URISyntaxException, IOException {
        RankCalculator calculator = fileWrapper.calculate(players, matchesCorrupted, DEFAULT_INITIAL_RANK, DEFAULT_K_FACTOR);
        calculator.generateFullRankReport();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testPlayersEmpty() throws URISyntaxException, IOException {
        RankCalculator calculator = fileWrapper.calculate(playersEmpty, matches, DEFAULT_INITIAL_RANK, DEFAULT_K_FACTOR);
        calculator.generateFullRankReport();
    }

    private int getPlayerPositionByID(PlayerRankWithPosition[] report, String id) {
        for (int i = 0; i < report.length; i++) {
            if (id.equals(report[i].getPlayer().getId())) {
                return i;
            }
        }
        return -1;
    }
}
