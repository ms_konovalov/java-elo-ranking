package ms.konovalov.elo;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class RankCalculatorTest {

    private Player ivan = new Player("1", "Ivan");
    private Player maria = new Player("2", "Maria");
    private Player eugene = new Player("33", "Eugene");
    private Player lewis = new Player("44", "Lewis");

    private Set<Player> players = new HashSet<Player>() {{
        add(ivan);
        add(maria);
        add(eugene);
        add(lewis);
    }};

    @Test
    public void addPlayer() throws Exception {
        RankCalculator calc = new RankCalculator(Collections.emptySet(), 1000, 42);
        players.forEach(calc::addPlayer);

        List<PlayerRankWithPosition> result = Arrays.asList(calc.generateFullRankReport());

        players.forEach(player ->
                Assert.assertTrue(result.stream().anyMatch(rank -> player.equals(rank.getPlayer())))
        );
    }

    @Test
    public void generatePersonalReport() throws Exception {
        RankCalculator calc = new RankCalculator(players, 1000, 42);
        calc.addMatches(Arrays.stream(new Match[]{
                new Match(ivan, maria),
                new Match(eugene, lewis),
                new Match(ivan, lewis)
        }));

        PlayerRankWithPosition ivanRank = calc.generatePersonalReport(ivan.getId()).get();
        Assert.assertEquals(2, ivanRank.getWinsCount());
        Assert.assertEquals(1, ivanRank.getPosition());

        PlayerRankWithPosition lewisRank = calc.generatePersonalReport(lewis.getId()).get();
        Assert.assertEquals(2, lewisRank.getLosesCount());
        Assert.assertEquals(4, lewisRank.getPosition());
    }

    @Test
    public void generateFullRankReport() throws Exception {
        RankCalculator calc = new RankCalculator(players, 1000, 42);
        calc.addMatches(Arrays.stream(new Match[]{
                new Match(ivan, maria),
                new Match(eugene, lewis),
        }));

        List<PlayerRankWithPosition> result = Arrays.asList(calc.generateFullRankReport());
        Assert.assertEquals(4, result.size());
    }

    @Test
    public void generateSuggestedMatches() throws Exception {
        RankCalculator calc = new RankCalculator(players, 1000, 42);
        List<Match> matches = Arrays.asList(calc.generateSuggestedMatches());
        Assert.assertEquals(2, matches.size());

        calc.addMatches(Arrays.stream(new Match[]{
                new Match(ivan, maria),
                new Match(ivan, lewis),
        }));
        matches = Arrays.asList(calc.generateSuggestedMatches());
        Assert.assertEquals(2, matches.size());
        Assert.assertTrue(matches.stream().anyMatch(m -> equals(m, ivan, eugene)));

        calc.addMatches(Arrays.stream(new Match[]{
                new Match(ivan, eugene),
                new Match(maria, lewis),
                new Match(maria, eugene),
                new Match(eugene, lewis),
        }));
        matches = Arrays.asList(calc.generateSuggestedMatches());
        Assert.assertEquals(2, matches.size());
        Assert.assertTrue(matches.stream().anyMatch(m ->
                equals(m, ivan, maria)
        ));

        Player tomas = new Player("qqq", "Tomas");
        calc.addPlayer(tomas);
        matches = Arrays.asList(calc.generateSuggestedMatches());
        Assert.assertEquals(2, matches.size());

        calc.addMatches(Arrays.stream(new Match[]{
                new Match(tomas, ivan),
                new Match(tomas, maria),
                new Match(tomas, eugene),
                new Match(tomas, lewis),
        }));
        matches = Arrays.asList(calc.generateSuggestedMatches());
        Assert.assertEquals(2, matches.size());
    }

    private boolean equals(Match m, Player first, Player second) {
        return m.getWinner().equals(first) && m.getLoser().equals(second);
    }


    @Test(expected = IllegalArgumentException.class)
    public void wrongMatchTest() {
        Player p1 = new Player("1", "");
        Player p2 = new Player("1", "123");
        new Match(p1, p2);
    }


}