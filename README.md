Elo Ranking calculator
======================

Implement a ranking program using the Elo ranking algorithm.  
 
Given two files:  
1. names, where each line has an ID number and a name for said ID.  
2. matches, where each line contains the ID of the two players of a match and the first one is the winner of said match.  
 
Implement a program that can read both files and then:  
1. Score each player based on the games played.  
2. Generate a list of players sorted by score, their ranking (position in the list) and their number of wins and losses.  
3. Generate a report for each person, showing with whom they played and how they fared.  
4. Generate a list of suggested next matches.  

Implementation
--------------

Solution is implemented with Java 8 as a console application. It uses Gradle as a build tool.  


Package
-------

To build solution, run unit-tests and generate test coverage report (Jacoco):  

```bash
./gradlew clean test jacocoTestReport
```

Test coverage report can be found at `./build/jacocoHtml/index.html`


Run application
---------------

Application can be run as a simple java app

```bash
java ms.konovalov.elo.EloRankApplication -p ./matches.txt -m ./players.txt -i 1000 -k 24
```

Also it can be started with gradle

```bash
./gradlew run -PappArgs="['-m', './matches.txt', '-p', './players.txt']"
```

Usage description available with arg `--help`

```bash
Usage: EloRankApplication [options]
  Options:
    --help

    --initialRating, -i
      initial rating for new players
      Default: 1000
    --kfactor, -k
      K factor for Elo algorithm
      Default: 32
  * --matches, -m
      path to file with list of played matches
  * --players, -p
      path to file with list of players
```

During program work it will calculate all the data and then ask about type of report you'd like to print

```bash
Choose which report do you want to print:
   Commands:
    full      print full rating report
      Usage: full

    personal      print personal rating report
      Usage: personal {player_id}

    recommended      print recommended matches
      Usage: recommended

    exit      exit
      Usage: exit
```

If print `full` then full rating of players will be printed

```
┌────┬──────────────────┬─────┬───┐
│Rank│Name              │Score│W/L│
├────┼──────────────────┼─────┼───┤
│1   │(ID=36) Jacquelynn│1064 │5/1│
│2   │(ID=31) Fernanda  │1048 │5/2│
│3   │(ID=13) Hunter    │1048 │5/2│
└────┴──────────────────┴─────┴───┘
```

If print `personal 36` then personal info about player with id=36 will be printed

```
┌────┬───────────────────────────────────────────┐
│Name│(ID=36) Jacquelynn                         │
├────┼─────────────────┬──────┬──────────────────┤
│Rank│1                │Rating│1064              │
├────┼─────────────────┼──────┼──────────────────┤
│Wins│(ID=22) Ileana   │Loses │(ID=20) Brianna   │
│    │(ID=30) Jaye     │      │                  │
│    │(ID=24) Jeanine  │      │                  │
│    │(ID=32) Augustine│      │                  │
│    │(ID=27) Inez     │      │                  │
└────┴─────────────────┴──────┴──────────────────┘
```

if print `recommended` then suggested matches will be printed

```
┌──┬──────────────────┬─────────────────┐
│1 │(ID=36) Jacquelynn│(ID=31) Fernanda │
│2 │(ID=13) Hunter    │(ID=39) Odette   │
│3 │(ID=5) Tai        │(ID=23) Denyse   │
└──┴──────────────────┴─────────────────┘
```

To finish application print `exit`